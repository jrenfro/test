package com.test.gwt_objectify_key_rpc.shared;

import java.io.Serializable;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;

@Entity(name="Foo")
public class FooEntity implements Serializable {
		
	private static final long serialVersionUID = 1L;
	
	public long id;
	public Key<FooEntity> bar;
	public com.google.appengine.api.datastore.Key raw;

	/** For GWT serialization **/
	public FooEntity() {}
	
	@Override
	public String toString() {
		return 
				this.getClass().getName() + " {" +
				"id:" + id + "; " +
				"bar:" + bar + "; " +
				"raw:" + raw + "}";
	}
}
