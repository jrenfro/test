package com.test.gwt_objectify_key_rpc.server;

import com.google.appengine.api.datastore.KeyFactory;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.test.gwt_objectify_key_rpc.client.RpcService;
import com.test.gwt_objectify_key_rpc.shared.FooEntity;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class RpcServiceImpl extends RemoteServiceServlet implements
		RpcService {

	@Override
	public FooEntity test(FooEntity foo) {
//		foo.raw = KeyFactory.createKey("Foo", foo.id);
//		foo.bar = Key.create(KeyFactory.createKey("Foo", 2L));
		return foo;
	}
}
