package com.test.gwt_objectify_key_rpc.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.test.gwt_objectify_key_rpc.shared.FooEntity;

/**
 * The async counterpart of <code>RpcService</code>.
 */
public interface RpcServiceAsync {
	void test(FooEntity foo, AsyncCallback<FooEntity> asyncCallback);
}
