package com.test.gwt_objectify_key_rpc.client;

import com.google.appengine.api.datastore.KeyFactory;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.googlecode.objectify.Key;
import com.test.gwt_objectify_key_rpc.shared.FooEntity;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Gwt_Objectify_Key_RPC implements EntryPoint {

	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	private final RpcServiceAsync rpcService = GWT
			.create(RpcService.class);

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		FooEntity foo = new FooEntity();
		FooEntity foo2 = new FooEntity();	

		foo.id = 1L;
		foo2.id = 2L;
		
		
		foo.raw = KeyFactory.createKey("Foo", foo.id);
//		foo2.raw = KeyFactory.createKey("FooEntity", foo2.id);
		
//		foo.bar = Key.create(KeyFactory.createKey("FooEntity", foo2.id));
		
		rpcService.test(foo, new AsyncCallback<FooEntity>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Failure! Message: " + caught.getMessage());
				caught.printStackTrace();
				
			}

			@Override
			public void onSuccess(FooEntity result) {
				Window.alert("Success! Entity: " + result);
				
			}});
	}
}
