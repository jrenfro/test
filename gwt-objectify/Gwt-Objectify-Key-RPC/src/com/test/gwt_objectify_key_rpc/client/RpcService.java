package com.test.gwt_objectify_key_rpc.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.test.gwt_objectify_key_rpc.shared.FooEntity;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("rpc")
public interface RpcService extends RemoteService {
	FooEntity test(FooEntity foo);
}
